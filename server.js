var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

var path = require('path');
app.use(express.static(__dirname + '/build/default'))
app.listen(port);

console.log('Waiting for Polymer-Node: ' + port);

app.get("/", function (req, res) {
  res.sendFile("index.html", {root:'.'});
});

app.post("/login", function (req, res) {
  console.log(req.params);
  res.json({hola: 'hola', email: req.body.email, password: req.body.password});
});
